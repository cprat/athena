// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/PackedLink.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2023
 * @brief Auxiliary variable type allowing to store @c ElementLinks
 *        as packed 32-bit values.
 *
 * (As yet incomplete.  Documentation will be added later.)
 */


#ifndef ATHCONTAINERS_PACKEDLINK_H
#define ATHCONTAINERS_PACKEDLINK_H


#include "AthContainers/PackedLinkImpl.h"
#include "AthContainers/tools/PackedLinkVectorFactory.h"
#include "AthContainers/PackedLinkConstAccessor.h"
#include "AthContainers/PackedLinkAccessor.h"
#include "AthContainers/PackedLinkDecorator.h"


#endif // not ATHCONTAINERS_PACKEDLINK_H
