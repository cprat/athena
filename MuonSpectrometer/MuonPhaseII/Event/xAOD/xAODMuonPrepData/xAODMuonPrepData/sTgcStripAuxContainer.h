/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_STGCSTRIPAUXCONTAINER_H
#define XAODMUONPREPDATA_STGCSTRIPAUXCONTAINER_H

#include "xAODMuonPrepData/sTgcStripClusterFwd.h"
#include "xAODMuonPrepData/versions/sTgcStripAuxContainer_v1.h"

// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::sTgcStripAuxContainer , 1320356011 , 1 )
#endif 